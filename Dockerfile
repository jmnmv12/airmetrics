FROM openjdk:11
COPY ./target/AirMetrics-0.0.1-SNAPSHOT.jar AirMetrics-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","AirMetrics-0.0.1-SNAPSHOT.jar"]