package com.tqs.airmetrics.webpagecontroller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebPageController {

    @GetMapping({ "/", "/home" })
    public String greeting( Model model) {

        return "greeting";
    }
    @GetMapping("/stats")
    public String cacheStatistics( Model model) {

        return "stats";
    }
    @GetMapping("/history")
    public String airMetricsHistory( Model model) {

        return "airmetricshistory";
    }

}