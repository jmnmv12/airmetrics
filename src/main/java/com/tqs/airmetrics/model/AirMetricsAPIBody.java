package com.tqs.airmetrics.model;


public class AirMetricsAPIBody {

    private AirMetrics airMetrics;
    private int cacheNumberOfRequests;
    private int cacheNumberOfHits;
    private int cacheNumberOfMisses;

    public AirMetricsAPIBody(AirMetrics airMetrics, int cacheNumberOfRequests, int cacheNumberOfHits, int cacheNumberOfMisses) {
        this.airMetrics = airMetrics;
        this.cacheNumberOfRequests = cacheNumberOfRequests;
        this.cacheNumberOfHits = cacheNumberOfHits;
        this.cacheNumberOfMisses = cacheNumberOfMisses;
    }

    public AirMetrics getAirMetrics() {
        return airMetrics;
    }

    public int getCacheNumberOfRequests() {
        return cacheNumberOfRequests;
    }

    public int getCacheNumberOfHits() {
        return cacheNumberOfHits;
    }

    public int getCacheNumberOfMisses() {
        return cacheNumberOfMisses;
    }

    public void setAirMetrics(AirMetrics airMetrics) {
        this.airMetrics = airMetrics;
    }
}

