package com.tqs.airmetrics.model;

public class City {

    private String id;
    private String text;
    private String latitude;
    private String longitude;

    public City() {}

    public City(String id, String text, Boolean flag) {
        if (Boolean.TRUE.equals(flag)) {
            this.id = id;
            this.text = text;
            this.latitude = "";
            this.longitude = "";
        } else {
            this.id = "";
            this.text = "";
            this.latitude = id;
            this.longitude = text;
        }


    }

    public City(String id, String text, String latitude, String longitude) {
        this.id = id;
        this.text = text;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

}
