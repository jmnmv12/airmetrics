package com.tqs.airmetrics.model;

public class CacheAPIBody {
    private int cacheNumberOfRequests;
    private int cacheNumberOfHits;
    private int cacheNumberOfMisses;

    public CacheAPIBody(int cacheNumberOfRequests, int cacheNumberOfHits, int cacheNumberOfMisses) {
        this.cacheNumberOfRequests = cacheNumberOfRequests;
        this.cacheNumberOfHits = cacheNumberOfHits;
        this.cacheNumberOfMisses = cacheNumberOfMisses;
    }

    public int getCacheNumberOfRequests() {
        return cacheNumberOfRequests;
    }

    public int getCacheNumberOfHits() {
        return cacheNumberOfHits;
    }

    public int getCacheNumberOfMisses() {
        return cacheNumberOfMisses;
    }
}
