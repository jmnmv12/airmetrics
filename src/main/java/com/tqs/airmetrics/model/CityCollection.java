package com.tqs.airmetrics.model;


import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.FileReader;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CityCollection {

    private HashMap<String, City> cityMap = new HashMap<>();
    Logger logger
            = Logger.getLogger(
            CityCollection.class.getName());

    public CityCollection(String csvFile) {

        //Load all the cities from the csv in to the collection
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] city = line.split(cvsSplitBy);
                City newCity= new City(city[0],city[1] + "," + city[3],city[5],city[6]);
                cityMap.put(city[0], newCity);


            }

        } catch (IOException e) {
            logger.log(Level.WARNING, e.toString());
        }

    }


    public Map<String, City> getCityMap() {
        return this.cityMap;
    }
}
