package com.tqs.airmetrics.model;


public class AirMetrics {

    private City city;
    private Double aqi; // Air Quality Index
    private Double o3; // Concentration of surface O3
    private Double so2; // Concentration of surface SO2
    private Double no2; // Concentration of surface NO2
    private Double co; // Concentration of carbon monoxide
    private Double pm25; // Concentration of particulate matter < 2.5 microns
    private Double pm10; // Concentration of particulate matter < 10 microns
    private String currentDate;

    public AirMetrics(){}

    public AirMetrics(City city, Double aqi, Double o3, Double so2, Double no2, Double co, Double pm25, Double pm10) {
        this.city = city;
        this.aqi = aqi;
        this.o3 = o3;
        this.so2 = so2;
        this.no2 = no2;
        this.co = co;
        this.pm25 = pm25;
        this.pm10 = pm10;
        this.currentDate="";
    }

    public AirMetrics(City city, Double aqi, Double o3, Double so2, Double no2, Double co, Double pm25, Double pm10, String currentDate) {
        this.city = city;
        this.aqi = aqi;
        this.o3 = o3;
        this.so2 = so2;
        this.no2 = no2;
        this.co = co;
        this.pm25 = pm25;
        this.pm10 = pm10;
        this.currentDate=currentDate;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public City getCity() {
        return city;
    }

    public Double getAqi() {
        return aqi;
    }

    public Double getO3() {
        return o3;
    }

    public Double getSo2() {
        return so2;
    }

    public Double getNo2() {
        return no2;
    }

    public Double getCo() {
        return co;
    }

    public Double getPm25() {
        return pm25;
    }

    public Double getPm10() {
        return pm10;
    }
}
