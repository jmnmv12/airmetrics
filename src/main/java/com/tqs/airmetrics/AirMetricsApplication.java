package com.tqs.airmetrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AirMetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirMetricsApplication.class, args);
	}

}
