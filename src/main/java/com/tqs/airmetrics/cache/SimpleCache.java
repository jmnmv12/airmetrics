package com.tqs.airmetrics.cache;

import com.tqs.airmetrics.model.AirMetrics;

import java.util.HashMap;
import java.util.Map;

public class SimpleCache {
    private Map<String, AirMetrics> objects;
    private Map<String, Long> expire;
    private long defaultExpire;
    private int numberOfRequests;
    private int numberOfHits;
    private int numberOfMisses;

    public SimpleCache(long defaultExpire) {
        this.objects = new HashMap<>();
        this.expire = new HashMap<>();

        this.defaultExpire = defaultExpire;
    }

    private void removeExpired(String name) {
        objects.remove(name);
        expire.remove(name);
    }


    public void put(String name, AirMetrics obj) {
        this.objects.put(name, obj);
        this.expire.put(name, System.currentTimeMillis() + this.defaultExpire * 1000);
    }




    public AirMetrics get(String name) {
        Long expireTime = this.expire.get(name);
        this.numberOfRequests++;

        if (expireTime == null) {
            this.numberOfMisses++;
            return null;
        }

        if (System.currentTimeMillis() > expireTime) {
            this.removeExpired(name);
            this.numberOfMisses++;
            return null;
        }
        this.numberOfHits++;
        return this.objects.get(name);
    }

    public int getNumberOfRequests() {
        return numberOfRequests;
    }

    public int getNumberOfHits() {
        return numberOfHits;
    }

    public int getNumberOfMisses() {
        return numberOfMisses;
    }
}
