package com.tqs.airmetrics.controller;

import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.service.AirMetricsService;
import com.tqs.airmetrics.exception.ResourceNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AirMetricsController {

    @Autowired
    private AirMetricsService airMetricsService;

    @GetMapping(path = "/air-metrics/{cityId}")
    public AirMetricsAPIBody getAirMetrics(@PathVariable(value = "cityId") String cityId) throws ResourceNotFound {
        AirMetricsAPIBody response = this.airMetricsService.getAirMetrics(cityId);

        if (response == null) {
            throw new ResourceNotFound("Invalid city ID.");
        }

        return response;
    }

    @GetMapping(path = "/air-metrics/history/{latitude},{longitude}/{date}")
    public AirMetricsAPIBody getHistoryAirMetrics(@PathVariable(value = "latitude") String latitude, @PathVariable(value = "longitude") String longitude, @PathVariable(value = "date") String requestedDate) throws ResourceNotFound {
        AirMetricsAPIBody response = this.airMetricsService.getAirMetricsHistory(latitude, longitude, requestedDate);

        if (response == null) {
            throw new ResourceNotFound("Invalid path variables.");
        }

        return response;
    }

    @GetMapping(path = "/cache-stats")
    public CacheAPIBody getCacheStats() {
        return this.airMetricsService.getCacheStats();
    }

}
