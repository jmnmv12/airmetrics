package com.tqs.airmetrics.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tqs.airmetrics.model.City;
import com.tqs.airmetrics.model.CityCollection;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
public class CityController {

    private CityCollection cityCollection;

    public CityController() {
        this.cityCollection = new CityCollection("src/main/resources/cities_full.csv");
    }

    @GetMapping(path="/city")
    public List<City> getCity(@RequestParam(value = "q", required = false) String query) {
        if (StringUtils.isEmpty(query)) {
            return cityCollection.getCityMap().entrySet().stream()
                    .limit(15)
                    .map(this::mapToCity)
                    .collect(Collectors.toList());
        }


        return cityCollection.getCityMap().entrySet().stream()
                .filter(city -> city.getValue().getText()
                        .contains(query))
                .limit(15)
                .map(this::mapToCity)
                .collect(Collectors.toList());
    }


    private City mapToCity(Map.Entry<String, City> entry) {
        return entry.getValue();
    }
}
