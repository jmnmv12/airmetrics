package com.tqs.airmetrics.service;

import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.repository.AirMetricsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AirMetricsService {

    @Autowired
    private AirMetricsRepository repository;

    public AirMetricsService(AirMetricsRepository repository) {
        this.repository=repository;
    }

    public AirMetricsAPIBody getAirMetrics(String cityId) {
        return repository.findByCityId(cityId);
    }

    public CacheAPIBody getCacheStats() {
        return repository.getCacheStats();
    }

    public AirMetricsAPIBody getAirMetricsHistory(String latitude,String longitude, String requestedDate) {
        return repository.findByLatitudeLongitudeDate(latitude,longitude,requestedDate);
    }
}
