package com.tqs.airmetrics.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tqs.airmetrics.cache.SimpleCache;
import com.tqs.airmetrics.model.AirMetrics;
import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.model.City;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class AirMetricsRepository {

    private static final String CONCENTRATION = "concentration";
    private static final String WHEATHERBITKEY = "e40772587a9f46439cb911d4e8b1c161";
    private static final String BREEZOMETERKEY = "1ab5d636e46d4223a728548d19bf228f";
    private static final String VALUE = "value";


    private SimpleCache myCache;
    private RestTemplate restTemplate;
    Logger logger
            = Logger.getLogger(
            AirMetricsRepository.class.getName());

    public AirMetricsRepository() {
        this.myCache = new SimpleCache(180);
        this.restTemplate = new RestTemplate();
    }

    public CacheAPIBody getCacheStats() {
        return new CacheAPIBody(myCache.getNumberOfRequests(), myCache.getNumberOfHits(), myCache.getNumberOfMisses());
    }

    public AirMetricsAPIBody findByCityId(String cityId) {
        AirMetrics found = myCache.get(cityId);
        AirMetricsAPIBody returnObject = new AirMetricsAPIBody(found, myCache.getNumberOfRequests(), myCache.getNumberOfHits(), myCache.getNumberOfMisses());


        if (found != null) {

            return returnObject;
        } else {
            if (!cityId.matches("^[0-9]+$"))
            {
                return null;
            }
            String airMetricsResourceUrl = "https://api.weatherbit.io/v2.0/current/airquality?city_id=" + cityId + "&key="+WHEATHERBITKEY;
            ResponseEntity<String> response ;

            try {

                response = restTemplate.getForEntity(airMetricsResourceUrl, String.class);
            } catch (HttpClientErrorException e) {
                logger.log(Level.WARNING, e.toString());
                return null;
            }

            if (response.getStatusCode() == HttpStatus.OK) {
                ObjectMapper mapper = new ObjectMapper();

                try {
                    JsonNode responseBody = mapper.readTree(response.getBody());
                    JsonNode data = responseBody.path("data");
                    JsonNode o3 = data.get(0).get("o3");
                    JsonNode so2 = data.get(0).get("so2");
                    JsonNode no2 = data.get(0).get("no2");
                    JsonNode aqi = data.get(0).get("aqi");
                    JsonNode co = data.get(0).get("co");
                    JsonNode pm10 = data.get(0).get("pm10");
                    JsonNode pm25 = data.get(0).get("pm25");
                    JsonNode cityName = responseBody.get("city_name");
                    JsonNode countryCode = responseBody.get("country_code");
                    City newCity = new City(cityId, cityName.asText() + "," + countryCode.asText(), true);

                    AirMetrics newAirMetrics = new AirMetrics(newCity, aqi.asDouble(), o3.asDouble(), so2.asDouble(), no2.asDouble(), co.asDouble(), pm25.asDouble(), pm10.asDouble());

                    // Put in cache
                    myCache.put(cityId, newAirMetrics);

                    // return Object
                    returnObject.setAirMetrics(newAirMetrics);
                    return returnObject;


                } catch (JsonProcessingException e) {
                    logger.log(Level.WARNING, e.toString());
                    return null;
                }


            }
        }

        return null;

    }

    public SimpleCache getMyCache() {
        return myCache;
    }

    public AirMetricsAPIBody findByLatitudeLongitudeDate(String latitude, String longitude, String requestedDate) {
        String cacheId = latitude + "," + longitude + "," + requestedDate;
        AirMetrics found = myCache.get(cacheId);

        AirMetricsAPIBody returnObject = new AirMetricsAPIBody(found, myCache.getNumberOfRequests(), myCache.getNumberOfHits(), myCache.getNumberOfMisses());

        if (found != null) {

            return returnObject;
        } else {
            if (!latitude.matches("^[0-9.]+$") || !longitude.matches("^[0-9.]+$") || !requestedDate.matches("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))"))
            {
                return null;
            }
            String airMetricsResourceUrl = "https://api.breezometer.com/air-quality/v2/historical/hourly?lat=" + latitude + "&lon=" + longitude + "&key="+BREEZOMETERKEY+"&datetime=" + requestedDate + "&features=breezometer_aqi,pollutants_concentrations";
            ResponseEntity<String> response;

            try {
                response = restTemplate.getForEntity(airMetricsResourceUrl, String.class);
            } catch (HttpClientErrorException e) {
                logger.log(Level.WARNING, e.toString());
                return null;
            }

            if (response.getStatusCode() == HttpStatus.OK) {
                ObjectMapper mapper = new ObjectMapper();

                try {
                    JsonNode responseBody = mapper.readTree(response.getBody());
                    JsonNode pollutants = responseBody.path("data").path("pollutants");
                    JsonNode o3 = pollutants.path("o3").path(CONCENTRATION).path(VALUE);
                    JsonNode so2 = pollutants.path("so2").path(CONCENTRATION).path(VALUE);
                    JsonNode no2 = pollutants.path("no2").path(CONCENTRATION).path(VALUE);
                    JsonNode aqi = responseBody.path("data").path("indexes").path("baqi").path("aqi");
                    JsonNode co = pollutants.path("co").path(CONCENTRATION).path(VALUE);
                    JsonNode pm10 = pollutants.path("pm10").path(CONCENTRATION).path(VALUE);
                    JsonNode pm25 = pollutants.path("pm25").path(CONCENTRATION).path(VALUE);

                    City newCity = new City(latitude, longitude, false);

                    AirMetrics newAirMetrics = new AirMetrics(newCity, aqi.asDouble(), o3.asDouble(), so2.asDouble(), no2.asDouble(), co.asDouble(), pm25.asDouble(), pm10.asDouble(), requestedDate);

                    // Put in cache
                    myCache.put(cacheId, newAirMetrics);

                    // return Object
                    returnObject.setAirMetrics(newAirMetrics);
                    return returnObject;


                } catch (JsonProcessingException e) {
                    logger.log(Level.WARNING, e.toString());
                    return null;
                }


            }
        }

        return null;
    }
}
