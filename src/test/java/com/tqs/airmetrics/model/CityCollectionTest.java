package com.tqs.airmetrics.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CityCollectionTest {

    @Test
    void getCityMap_givingInvalidFile() {

        CityCollection cityCollection=new CityCollection("invalid_file");
        assertThat(cityCollection.getCityMap()).isEmpty();
    }

    @Test
    void getCityMap_givingValidFile() {

        CityCollection cityCollection=new CityCollection("src/main/resources/cities_full.csv");

        assertThat(cityCollection.getCityMap()).isNotEmpty();
    }
}