package com.tqs.airmetrics.repository;

import com.tqs.airmetrics.cache.SimpleCache;
import com.tqs.airmetrics.model.AirMetrics;
import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.model.City;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class AirMetricsRepositoryTest {

    @Mock(lenient = true)
    private SimpleCache cache;

    @Mock(lenient = true)
    private RestTemplate restTemplate;

    @InjectMocks
    private AirMetricsRepository repository;

    private static final String WHEATHERBITKEY = "e40772587a9f46439cb911d4e8b1c161";
    private static final String BREEZOMETERKEY = "1ab5d636e46d4223a728548d19bf228f";


    @Test
    void findByCityId_inCache() {
        String cityId = "ABC";
        AirMetrics sjmAirMetrics = new AirMetrics(new City("ABC", "São João da Madeira,PT", true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        Mockito.when(cache.get(cityId)).thenReturn(sjmAirMetrics);

        AirMetricsAPIBody found = repository.findByCityId("ABC");

        assertThat(found.getAirMetrics().getAqi()).isEqualTo(sjmAirMetrics.getAqi());


    }

    @Test
    void findByCityId_notInCache() {
        String cityId = "2734484";
        String airMetricsResourceUrl = "https://api.weatherbit.io/v2.0/current/airquality?city_id=" + cityId + "&key="+WHEATHERBITKEY;
        ResponseEntity<String> response = new ResponseEntity<>("{\"data\":[{\"mold_level\":0,\"aqi\":29,\"pm10\":17,\"co\":419,\"o3\":62.3333,\"predominant_pollen_type\":\"Trees\",\"so2\":10.6667,\"pollen_level_tree\":2,\"pollen_level_weed\":0,\"no2\":3.66667,\"pm25\":2,\"pollen_level_grass\":0}],\"city_name\":\"São João da Madeira\",\"lon\":\"-8.48969\",\"timezone\":\"Europe\\/Lisbon\",\"lat\":\"40.90229\",\"country_code\":\"PT\",\"state_code\":\"02\"}\n", HttpStatus.OK);

        Mockito.when(cache.get(cityId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenReturn(response);

        AirMetricsAPIBody found = repository.findByCityId("2734484");

        // Cache is empty but the repository still returns a object
        assertThat(found).isNotNull();

    }

    @Test
    void findByInvalidCityIdFormat_returnsNull() {
        String cityId = "2734484ABC";
        String airMetricsResourceUrl = "https://api.weatherbit.io/v2.0/current/airquality?city_id=" + cityId + "&key="+WHEATHERBITKEY;
        ResponseEntity<String> response = new ResponseEntity<>("", HttpStatus.NOT_FOUND);

        Mockito.when(cache.get(cityId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenReturn(response);

        AirMetricsAPIBody found = repository.findByCityId(cityId);

        assertThat(found).isNull();


    }

    @Test
    void findByInvalidCityId_returnsNull() {
        String cityId = "000";
        String airMetricsResourceUrl = "https://api.weatherbit.io/v2.0/current/airquality?city_id=" + cityId + "&key="+WHEATHERBITKEY;
        ResponseEntity<String> response = new ResponseEntity<>("", HttpStatus.NOT_FOUND);

        Mockito.when(cache.get(cityId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenReturn(response);

        AirMetricsAPIBody found = repository.findByCityId(cityId);

        assertThat(found).isNull();

        Mockito.verify(restTemplate, VerificationModeFactory.times(1)).getForEntity(airMetricsResourceUrl, String.class);
    }

    @Test
    void findByValidCityIdInvalidURL_throwsExceptionReturnsNull() {
        String cityId = "2734484";
        String airMetricsResourceUrl = "https://api.weatherbit.io/v2.0/current/airquality?city_id=" + cityId + "&key="+WHEATHERBITKEY;

        Mockito.when(cache.get(cityId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenThrow(HttpClientErrorException.class);


        AirMetricsAPIBody found = repository.findByCityId(cityId);

        assertThat(found).isNull();

    }

    @Test
    void findByLatitudeLongitudeDate_inCache() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2020-04-08";
        String cacheId = latitude + "," + longitude + "," + requestedDate;
        AirMetrics historyAirMetrics = new AirMetrics(new City(latitude, longitude, false), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, "2020-04-08");


        Mockito.when(this.cache.get(cacheId)).thenReturn(historyAirMetrics);

        AirMetricsAPIBody found = this.repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found.getAirMetrics().getAqi()).isEqualTo(historyAirMetrics.getAqi());


    }

    @Test
    void findByLatitudeLongitudeDate_notInCache() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2020-04-08";
        String cacheId = latitude + "," + longitude + "," + requestedDate;
        String airMetricsResourceUrl = "https://api.breezometer.com/air-quality/v2/historical/hourly?lat=" + latitude + "&lon=" + longitude + "&key="+BREEZOMETERKEY+"&datetime=" + requestedDate + "&features=breezometer_aqi,pollutants_concentrations";


        ResponseEntity<String> response = new ResponseEntity<>("{\"metadata\": null, \"data\": {\"datetime\": \"2020-04-08T00:00:00Z\", \"data_available\": true, \"indexes\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 55, \"aqi_display\": \"55\", \"color\": \"#E0F30C\", \"category\": \"Moderate air quality\", \"dominant_pollutant\": \"pm25\"}}, \"pollutants\": {\"co\": {\"display_name\": \"CO\", \"full_name\": \"Carbon monoxide\", \"concentration\": {\"value\": 494.76, \"units\": \"ppb\"}}, \"no2\": {\"display_name\": \"NO2\", \"full_name\": \"Nitrogen dioxide\", \"concentration\": {\"value\": 24.11, \"units\": \"ppb\"}}, \"o3\": {\"display_name\": \"O3\", \"full_name\": \"Ozone\", \"concentration\": {\"value\": 16.86, \"units\": \"ppb\"}}, \"pm10\": {\"display_name\": \"PM10\", \"full_name\": \"Inhalable particulate matter (<10\\u00b5m)\", \"concentration\": {\"value\": 30.9, \"units\": \"ug/m3\"}}, \"pm25\": {\"display_name\": \"PM2.5\", \"full_name\": \"Fine particulate matter (<2.5\\u00b5m)\", \"concentration\": {\"value\": 29.27, \"units\": \"ug/m3\"}}, \"so2\": {\"display_name\": \"SO2\", \"full_name\": \"Sulfur dioxide\", \"concentration\": {\"value\": 0.42, \"units\": \"ppb\"}}}}, \"error\": null}", HttpStatus.OK);

        Mockito.when(cache.get(cacheId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenReturn(response);

        AirMetricsAPIBody found = repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found).isNotNull();


    }

    @Test
    void findByInvalidDateFormat_returnsNull() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2021-04-8T";
        String cacheId = latitude + "," + longitude + "," + requestedDate;
        String airMetricsResourceUrl = "https://api.breezometer.com/air-quality/v2/historical/hourly?lat=" + latitude + "&lon=" + longitude + "&key="+BREEZOMETERKEY+"&datetime=" + requestedDate + "&features=breezometer_aqi,pollutants_concentrations";


        ResponseEntity<String> response = new ResponseEntity<>("", HttpStatus.NOT_FOUND);

        Mockito.when(cache.get(cacheId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenReturn(response);


        AirMetricsAPIBody found = repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found).isNull();


    }

    @Test
    void findByInvalidDate_returnsNull() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2021-04-08";
        String cacheId = latitude + "," + longitude + "," + requestedDate;
        String airMetricsResourceUrl = "https://api.breezometer.com/air-quality/v2/historical/hourly?lat=" + latitude + "&lon=" + longitude + "&key="+BREEZOMETERKEY+"&datetime=" + requestedDate + "&features=breezometer_aqi,pollutants_concentrations";


        ResponseEntity<String> response = new ResponseEntity<>("", HttpStatus.NOT_FOUND);

        Mockito.when(cache.get(cacheId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenReturn(response);


        AirMetricsAPIBody found = repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found).isNull();

        Mockito.verify(restTemplate, VerificationModeFactory.times(1)).getForEntity(airMetricsResourceUrl, String.class);


    }

    @Test
    void findByInvalidLongitudeFormat_returnsNull() {
        String latitude = "48.857456";
        String longitude = "2.354611AB";
        String requestedDate = "2020-04-08";
        String cacheId = latitude + "," + longitude + "," + requestedDate;


        Mockito.when(cache.get(cacheId)).thenReturn(null);


        AirMetricsAPIBody found = repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found).isNull();


    }

    @Test
    void findByInvalidLatitudeFormat_returnsNull() {
        String latitude = "48.857456AB";
        String longitude = "2.354611";
        String requestedDate = "2020-04-08";
        String cacheId = latitude + "," + longitude + "," + requestedDate;

        Mockito.when(cache.get(cacheId)).thenReturn(null);

        AirMetricsAPIBody found = repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found).isNull();


    }

    @Test
    void findByLatitudeLongitudeDateInvalidURL_throwsExceptionReturnsNull() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2020-04-08";
        String cacheId = latitude + "," + longitude + "," + requestedDate;
        String airMetricsResourceUrl = "https://api.breezometer.com/air-quality/v2/historical/hourly?lat=" + latitude + "&lon=" + longitude + "&key="+BREEZOMETERKEY+"&datetime=" + requestedDate + "&features=breezometer_aqi,pollutants_concentrations";

        Mockito.when(cache.get(cacheId)).thenReturn(null);

        Mockito.when(restTemplate.getForEntity(airMetricsResourceUrl, String.class)).thenThrow(HttpClientErrorException.class);

        AirMetricsAPIBody found = repository.findByLatitudeLongitudeDate(latitude, longitude, requestedDate);

        assertThat(found).isNull();

        Mockito.verify(restTemplate, VerificationModeFactory.times(1)).getForEntity(airMetricsResourceUrl, String.class);

    }


    @Test
    void getCacheStats_returnsCacheAPIBody() {

        Mockito.when(cache.getNumberOfRequests()).thenReturn(0);
        Mockito.when(cache.getNumberOfHits()).thenReturn(0);
        Mockito.when(cache.getNumberOfMisses()).thenReturn(0);

        CacheAPIBody found = repository.getCacheStats();

        // Cache always returns the stats
        assertThat(found).isNotNull();

    }

}