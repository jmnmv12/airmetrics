package com.tqs.airmetrics.service;

import com.tqs.airmetrics.model.AirMetrics;
import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.model.City;
import com.tqs.airmetrics.repository.AirMetricsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class AirMetricsServiceUnitTest {

    @Mock(lenient = true)
    private AirMetricsRepository repository;

    @InjectMocks
    private AirMetricsService service;

    @BeforeEach
    public void setUp() {
        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT", true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);
        AirMetricsAPIBody returnObject = new AirMetricsAPIBody(sjmAirMetrics, 0, 0, 0);

        Mockito.when(repository.findByCityId(sjmAirMetrics.getCity().getId())).thenReturn(returnObject);

        AirMetrics historyAirMetrics = new AirMetrics(new City("48.857456", "2.354611", false), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, "2020-04-08");
        AirMetricsAPIBody returnObjectHistory = new AirMetricsAPIBody(historyAirMetrics, 0, 0, 0);

        Mockito.when(repository.findByLatitudeLongitudeDate(historyAirMetrics.getCity().getLatitude(), historyAirMetrics.getCity().getLongitude(), historyAirMetrics.getCurrentDate())).thenReturn(returnObjectHistory);

        CacheAPIBody returnObjectCache = new CacheAPIBody(2, 0, 2);

        Mockito.when(repository.getCacheStats()).thenReturn(returnObjectCache);

    }

    @Test
    void whenValidLatitudeLongitudeDate_thenAirMetricsHistoryShouldBeFound() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2020-04-08";

        AirMetricsAPIBody found = service.getAirMetricsHistory(latitude, longitude, requestedDate);

        assertThat(found.getAirMetrics().getCity().getLatitude()).isEqualTo(latitude);
        assertThat(found.getAirMetrics().getCity().getLongitude()).isEqualTo(longitude);
        assertThat(found.getAirMetrics().getCurrentDate()).isEqualTo(requestedDate);

        verifyFindByLatitudeLongitudeDateIsCalledOnce(latitude, longitude, requestedDate);



    }

    @Test
    void whenInvalidDate_thenAirMetricsHistoryShouldNotBeFound() {
        String latitude = "48.857456";
        String longitude = "2.354611";
        String requestedDate = "2020-04-12T10:00:00";

        AirMetricsAPIBody found = service.getAirMetricsHistory(latitude, longitude, requestedDate);

        assertThat(found).isNull();


        verifyFindByLatitudeLongitudeDateIsCalledOnce(latitude, longitude, requestedDate);


    }

    @Test
    void whenInvalidLatitude_thenAirMetricsHistoryShouldNotBeFound() {
        String latitude = "48.857456AB";
        String longitude = "2.354611";
        String requestedDate = "2020-04-08";

        AirMetricsAPIBody found = service.getAirMetricsHistory(latitude, longitude, requestedDate);

        assertThat(found).isNull();

        verifyFindByLatitudeLongitudeDateIsCalledOnce(latitude, longitude, requestedDate);


    }

    @Test
    void whenInvalidLongitude_thenAirMetricsHistoryShouldNotBeFound() {
        String latitude = "48.857456";
        String longitude = "2.354611AB";
        String requestedDate = "2020-04-08";

        AirMetricsAPIBody found = service.getAirMetricsHistory(latitude, longitude, requestedDate);

        assertThat(found).isNull();

        verifyFindByLatitudeLongitudeDateIsCalledOnce(latitude, longitude, requestedDate);


    }

    @Test
    void whenValidCityId_thenAirMetricsShouldBeFound() {
        String cityId = "2734484";

        AirMetricsAPIBody found = service.getAirMetrics(cityId);

        assertThat(found.getAirMetrics().getCity().getId()).isEqualTo(cityId);

        verifyFindByCityIdIsCalledOnce(cityId);

    }

    @Test
    void whenInvalidCityId_thenAirMetricsShouldNotExist() {
        String cityId = "ABC";

        AirMetricsAPIBody found = service.getAirMetrics(cityId);

        assertThat(found).isNull();

        verifyFindByCityIdIsCalledOnce(cityId);
    }

    @Test
    void whenCacheStatsIsCalled_thenCacheStatsShouldBeFound() {
        CacheAPIBody found = service.getCacheStats();

        assertThat(found).isNotNull();

        verifyGetCacheStatsIsCalledOnce();
    }

    private void verifyFindByLatitudeLongitudeDateIsCalledOnce(String latitude, String longitude, String requestedDate) {
        Mockito.verify(repository, VerificationModeFactory.times(1)).findByLatitudeLongitudeDate(latitude, longitude, requestedDate);
        Mockito.reset(repository);
    }

    private void verifyFindByCityIdIsCalledOnce(String cityId) {
        Mockito.verify(repository, VerificationModeFactory.times(1)).findByCityId(cityId);
        Mockito.reset(repository);
    }

    private void verifyGetCacheStatsIsCalledOnce() {
        Mockito.verify(repository, VerificationModeFactory.times(1)).getCacheStats();
        Mockito.reset(repository);
    }
}