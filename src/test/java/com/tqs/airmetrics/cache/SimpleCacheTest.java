package com.tqs.airmetrics.cache;

import com.tqs.airmetrics.model.AirMetrics;
import com.tqs.airmetrics.model.City;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

class SimpleCacheTest {


    @BeforeEach
    public void setUp() {

    }

    @Test
    void getObjectBeforeItExpires() throws Exception {
        SimpleCache myCache = new SimpleCache(5);

        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        myCache.put(sjmAirMetrics.getCity().getId(), sjmAirMetrics);

        //Sleep for 3 seconds
        await().pollDelay(3,TimeUnit.SECONDS).until(() -> true);

        AirMetrics found = myCache.get(sjmAirMetrics.getCity().getId());

        assertThat(found).isEqualTo(sjmAirMetrics);

    }

    @Test
    void getObjectAfterExpires_returnsNull() throws Exception {
        SimpleCache myCache = new SimpleCache(3);

        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        myCache.put(sjmAirMetrics.getCity().getId(), sjmAirMetrics);

        //Sleep for 4 seconds
        await().pollDelay(4,TimeUnit.SECONDS).until(() -> true);

        AirMetrics found = myCache.get(sjmAirMetrics.getCity().getId());

        assertThat(found).isNull();

    }

    @Test
    void get2_ObjectsBeforeExpires() throws Exception {
        SimpleCache myCache = new SimpleCache(3);

        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);
        AirMetrics feiraAirMetrics = new AirMetrics(new City("2734485", "Feira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        myCache.put(sjmAirMetrics.getCity().getId(), sjmAirMetrics);
        myCache.put(feiraAirMetrics.getCity().getId(), feiraAirMetrics);

        //Sleep for 1 seconds
        await().pollDelay(1,TimeUnit.SECONDS).until(() -> true);


        myCache.get(sjmAirMetrics.getCity().getId());
        myCache.get(feiraAirMetrics.getCity().getId());

        int numberOfHits = myCache.getNumberOfHits();
        int numberOfRequests = myCache.getNumberOfRequests();

        assertThat(numberOfHits).isEqualTo(2);
        assertThat(numberOfRequests).isEqualTo(2);


    }

    @Test
    void get1_ObjectBeforeItExpires() throws Exception {
        SimpleCache myCache = new SimpleCache(3);

        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);
        AirMetrics feiraAirMetrics = new AirMetrics(new City("2734485", "Feira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        myCache.put(sjmAirMetrics.getCity().getId(), sjmAirMetrics);
        myCache.put(feiraAirMetrics.getCity().getId(), feiraAirMetrics);

        await().pollDelay(1,TimeUnit.SECONDS).until(() -> true);

        myCache.get(sjmAirMetrics.getCity().getId());

        await().pollDelay(4,TimeUnit.SECONDS).until(() -> true);

        myCache.get(feiraAirMetrics.getCity().getId());

        int numberOfHits = myCache.getNumberOfHits();
        int numberOfMisses = myCache.getNumberOfMisses();
        int numberOfRequests = myCache.getNumberOfRequests();

        assertThat(numberOfHits).isEqualTo(1);
        assertThat(numberOfMisses).isEqualTo(1);
        assertThat(numberOfRequests).isEqualTo(2);


    }

    @Test
    void getObjectInCacheInitialization_returnsNull() throws Exception {
        SimpleCache myCache = new SimpleCache(3);

        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        myCache.get(sjmAirMetrics.getCity().getId());


        int numberOfHits = myCache.getNumberOfHits();
        int numberOfMisses = myCache.getNumberOfMisses();
        int numberOfRequests = myCache.getNumberOfRequests();

        assertThat(numberOfHits).isEqualTo(0);
        assertThat(numberOfMisses).isEqualTo(1);
        assertThat(numberOfRequests).isEqualTo(1);


    }




}