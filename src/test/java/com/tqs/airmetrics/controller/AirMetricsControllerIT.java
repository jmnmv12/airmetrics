package com.tqs.airmetrics.controller;

import com.tqs.airmetrics.AirMetricsApplication;
import com.tqs.airmetrics.model.AirMetrics;
import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.model.City;
import com.tqs.airmetrics.repository.AirMetricsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = AirMetricsApplication.class)
@AutoConfigureMockMvc
class AirMetricsControllerIT {

    @Autowired
    private AirMetricsRepository repository;

    @Autowired
    private TestRestTemplate restClient; // External REST client


    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void givenCityId_whenGetAirMetrics_thenReturnValueInCache() throws Exception {
        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT",true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);

        repository.getMyCache().put("2734484", sjmAirMetrics);

        ResponseEntity<AirMetricsAPIBody> entity = restClient.getForEntity("/api/air-metrics/2734484", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getAirMetrics().getCity().getId()).isEqualTo("2734484");
        assertThat(entity.getBody().getCacheNumberOfHits()).isEqualTo(1);
        assertThat(entity.getBody().getCacheNumberOfRequests()).isEqualTo(1);
        assertThat(entity.getBody().getCacheNumberOfMisses()).isEqualTo(0);
    }

    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void givenCityId_whenGetAirMetrics_thenReturnValueNotInCache() throws Exception {
        AirMetrics sjmAirMetrics = new AirMetrics(new City("48.857456", "2.354611",false), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, "2020-04-08");

        repository.getMyCache().put("48.857456,2.354611,2020-04-08", sjmAirMetrics);

        ResponseEntity<AirMetricsAPIBody> entity = restClient.getForEntity("/api/air-metrics/2734484", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getAirMetrics().getCity().getId()).isEqualTo("2734484");
        assertThat(entity.getBody().getCacheNumberOfHits()).isEqualTo(0);
        assertThat(entity.getBody().getCacheNumberOfRequests()).isEqualTo(1);
        assertThat(entity.getBody().getCacheNumberOfMisses()).isEqualTo(1);
    }

    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void givenInvalidCityId_whenGetAirMetrics_thenReturnValueNotFound() throws Exception {

        ResponseEntity<AirMetricsAPIBody> entity = restClient.getForEntity("/api/air-metrics/2734484AB", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }

    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void givenLatitudeLongitudeDate_whenGetAirMetricsHistory_thenReturnValueNotInCache() throws Exception {

        ResponseEntity<AirMetricsAPIBody> entity = restClient.getForEntity("/api/air-metrics/history/48.857456,2.354611/2020-04-08", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getAirMetrics().getCity().getLatitude()).isEqualTo("48.857456");
        assertThat(entity.getBody().getCacheNumberOfHits()).isEqualTo(0);
        assertThat(entity.getBody().getCacheNumberOfRequests()).isEqualTo(1);
        assertThat(entity.getBody().getCacheNumberOfMisses()).isEqualTo(1);
    }

    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void givenInvalidParameters_whenGetAirMetricsHistory_thenReturnNotFound() throws Exception {

        ResponseEntity<AirMetricsAPIBody> entity = restClient.getForEntity("/api/air-metrics/history/48.857456A,2.354611/2020-04-08", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        entity = restClient.getForEntity("/api/air-metrics/history/48.857456,2.354611A/2020-04-08", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        entity = restClient.getForEntity("/api/air-metrics/history/48.857456,2.354611/2020-04/8T10:00:00", AirMetricsAPIBody.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void givenLatitudeLongitudeDate_whenGetAirMetricsHistory_thenReturnValueInCache() throws Exception {
        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT","48.857456","2.354611"), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00,"2020-04-08");

        repository.getMyCache().put("48.857456,2.354611,2020-04-08", sjmAirMetrics);


        ResponseEntity<AirMetricsAPIBody> entity = restClient.getForEntity("/api/air-metrics/history/48.857456,2.354611/2020-04-08", AirMetricsAPIBody.class);


        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getAirMetrics().getCity().getLatitude()).isEqualTo("48.857456");
        assertThat(entity.getBody().getCacheNumberOfHits()).isEqualTo(1);
        assertThat(entity.getBody().getCacheNumberOfRequests()).isEqualTo(1);
        assertThat(entity.getBody().getCacheNumberOfMisses()).isEqualTo(0);
    }

    @DirtiesContext(classMode=ClassMode.AFTER_CLASS) // It assures the Spring ApplicationContext will be reloaded after the execution of the test, so we get a new instance of repository
    @Test
    public void whenGetCacheStats_thenReturnCorrectStats() throws Exception {

        // Calls /air-metrics for city 2734484 once
        restClient.getForEntity("/api/air-metrics/2734484", AirMetricsAPIBody.class);

        ResponseEntity<CacheAPIBody> entity2 = restClient.getForEntity("/api/cache-stats/", CacheAPIBody.class);

        assertThat(entity2.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity2.getBody().getCacheNumberOfHits()).isEqualTo(0);
        assertThat(entity2.getBody().getCacheNumberOfRequests()).isEqualTo(1);
        assertThat(entity2.getBody().getCacheNumberOfMisses()).isEqualTo(1);

        // Calls /air-metrics for city 2734484 again
        restClient.getForEntity("/api/air-metrics/2734484", AirMetricsAPIBody.class);

        entity2 = restClient.getForEntity("/api/cache-stats/", CacheAPIBody.class);

        assertThat(entity2.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity2.getBody().getCacheNumberOfHits()).isEqualTo(1);
        assertThat(entity2.getBody().getCacheNumberOfRequests()).isEqualTo(2);
        assertThat(entity2.getBody().getCacheNumberOfMisses()).isEqualTo(1);

    }
}