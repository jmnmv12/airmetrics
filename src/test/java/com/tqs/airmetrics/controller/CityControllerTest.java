package com.tqs.airmetrics.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CityController.class)
class CityControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void givenCityName_WhenGetCity_thenReturnJsonArray() throws Exception {
        // Because we load all the cities from a csv that we know,this endpoint will give the following cityName and cityCode:
        // cityName=São João da Madeira,PT
        // cityCode=2734484
        String cityName="São João da Madeira,PT";
        String cityCode="2734484";

        mvc.perform(get("/api/city?q=São João da Madeira").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$[0].id", is(cityCode))).andExpect(jsonPath("$[0].text", is(cityName)));

    }

    @Test
    void givenNonExistingCityName_WhenGetCity_thenEmptyReturnJsonArray() throws Exception {

        mvc.perform(get("/api/city?q=ABCCDEFG").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(0)));

    }

    @Test
    void notGivenCityName_WhenGetCity_thenReturnJsonArray() throws Exception {
        // Sample response for q= will have size 15

        mvc.perform(get("/api/city?q=").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(15)));

    }
}