package com.tqs.airmetrics.controller;

import com.tqs.airmetrics.model.AirMetrics;
import com.tqs.airmetrics.model.AirMetricsAPIBody;
import com.tqs.airmetrics.model.CacheAPIBody;
import com.tqs.airmetrics.model.City;
import com.tqs.airmetrics.service.AirMetricsService;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AirMetricsController.class)
class AirMetricsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AirMetricsService service;

    @Test
    public void givenCityId_whenGetAirMetrics_thenReturnJsonArray() throws Exception {
        AirMetrics sjmAirMetrics = new AirMetrics(new City("2734484", "São João da Madeira,PT", true), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00);
        AirMetricsAPIBody returnObject = new AirMetricsAPIBody(sjmAirMetrics, 0, 0, 0);

        given(service.getAirMetrics("123")).willReturn(returnObject);

        mvc.perform(get("/api/air-metrics/123").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.airMetrics.city.id", is(sjmAirMetrics.getCity().getId()))).andExpect(jsonPath("$.airMetrics.city.text", is(sjmAirMetrics.getCity().getText()))).andExpect(jsonPath("$.airMetrics.aqi", is(sjmAirMetrics.getAqi()))).andExpect(jsonPath("$.airMetrics.o3", is(sjmAirMetrics.getO3()))).andExpect(jsonPath("$.airMetrics.so2", is(sjmAirMetrics.getSo2()))).andExpect(jsonPath("$.airMetrics.no2", is(sjmAirMetrics.getNo2()))).andExpect(jsonPath("$.airMetrics.co", is(sjmAirMetrics.getCo()))).andExpect(jsonPath("$.airMetrics.pm25", is(sjmAirMetrics.getPm25()))).andExpect(jsonPath("$.airMetrics.pm10", is(sjmAirMetrics.getPm10()))).andExpect(jsonPath("$.cacheNumberOfRequests", is(returnObject.getCacheNumberOfRequests()))).andExpect(jsonPath("$.cacheNumberOfHits", is(returnObject.getCacheNumberOfHits()))).andExpect(jsonPath("$.cacheNumberOfMisses", is(returnObject.getCacheNumberOfMisses())));

        verify(service, VerificationModeFactory.times(1)).getAirMetrics("123");
        reset(service);

    }

    @Test
    public void givenInvalidCityId_whenGetAirMetrics_thenReturnError() throws Exception {


        given(service.getAirMetrics("ABC")).willReturn(null);

        mvc.perform(get("/api/air-metrics/ABC").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());

        verify(service, VerificationModeFactory.times(1)).getAirMetrics("ABC");
        reset(service);

    }

    @Test
    public void whenGetAirMetricsCacheStats_thenReturnJsonArray() throws Exception {
        CacheAPIBody returnObject = new CacheAPIBody(2, 0, 2);

        given(service.getCacheStats()).willReturn(returnObject);

        mvc.perform(get("/api/cache-stats").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.cacheNumberOfRequests", is(returnObject.getCacheNumberOfRequests()))).andExpect(jsonPath("$.cacheNumberOfHits", is(returnObject.getCacheNumberOfHits()))).andExpect(jsonPath("$.cacheNumberOfMisses", is(returnObject.getCacheNumberOfMisses())));

        verify(service, VerificationModeFactory.times(1)).getCacheStats();
        reset(service);

    }

    @Test
    public void givenLatitudeLongitudeDate_whenGetAirMetricsHistory__thenReturnJsonArray() throws Exception {
        AirMetrics sjmAirMetrics = new AirMetrics(new City("48.857456", "2.354611", false), 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, 30.00, "2020-04-08");
        AirMetricsAPIBody returnObject = new AirMetricsAPIBody(sjmAirMetrics, 0, 0, 0);

        given(service.getAirMetricsHistory("48.857456", "2.354611", "2020-04-08")).willReturn(returnObject);

        mvc.perform(get("/api/air-metrics/history/48.857456,2.354611/2020-04-08").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.airMetrics.city.latitude", is(sjmAirMetrics.getCity().getLatitude()))).andExpect(jsonPath("$.airMetrics.city.longitude", is(sjmAirMetrics.getCity().getLongitude()))).andExpect(jsonPath("$.airMetrics.aqi", is(sjmAirMetrics.getAqi()))).andExpect(jsonPath("$.airMetrics.o3", is(sjmAirMetrics.getO3()))).andExpect(jsonPath("$.airMetrics.so2", is(sjmAirMetrics.getSo2()))).andExpect(jsonPath("$.airMetrics.no2", is(sjmAirMetrics.getNo2()))).andExpect(jsonPath("$.airMetrics.co", is(sjmAirMetrics.getCo()))).andExpect(jsonPath("$.airMetrics.pm25", is(sjmAirMetrics.getPm25()))).andExpect(jsonPath("$.airMetrics.pm10", is(sjmAirMetrics.getPm10()))).andExpect(jsonPath("$.airMetrics.currentDate", is(sjmAirMetrics.getCurrentDate()))).andExpect(jsonPath("$.cacheNumberOfRequests", is(returnObject.getCacheNumberOfRequests()))).andExpect(jsonPath("$.cacheNumberOfHits", is(returnObject.getCacheNumberOfHits()))).andExpect(jsonPath("$.cacheNumberOfMisses", is(returnObject.getCacheNumberOfMisses())));

        verify(service, VerificationModeFactory.times(1)).getAirMetricsHistory("48.857456", "2.354611", "2020-04-08");
        reset(service);

    }

    @Test
    public void givenInvalidDate_whenGetAirMetricsHistory__thenError() throws Exception {

        given(service.getAirMetricsHistory("48.857456", "2.354611", "2020-06-8T10:00:00")).willReturn(null);

        mvc.perform(get("/api/air-metrics/history/48.857456,2.354611/2020-06-8T10:00:00").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());

        verify(service, VerificationModeFactory.times(1)).getAirMetricsHistory("48.857456", "2.354611", "2020-06-8T10:00:00");
        reset(service);

    }

    @Test
    public void givenInvalidLatitude_whenGetAirMetricsHistory__thenError() throws Exception {

        given(service.getAirMetricsHistory("48.857456AB", "2.354611", "2020-04-08")).willReturn(null);

        mvc.perform(get("/api/air-metrics/history/48.857456AB,2.354611/2020-04-08").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());

        verify(service, VerificationModeFactory.times(1)).getAirMetricsHistory("48.857456AB", "2.354611", "2020-04-08");
        reset(service);

    }

    @Test
    public void givenInvalidLongitude_whenGetAirMetricsHistory__thenError() throws Exception {

        given(service.getAirMetricsHistory("48.857456", "2.354611AB", "2020-04-08")).willReturn(null);

        mvc.perform(get("/api/air-metrics/history/48.857456,2.354611AB/2020-04-08").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());

        verify(service, VerificationModeFactory.times(1)).getAirMetricsHistory("48.857456", "2.354611AB", "2020-04-08");
        reset(service);

    }

}